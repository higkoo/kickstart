#!/bin/bash -x

sDisk=/mnt/CentOS-6.4-Default
dDisk=/data/appdata/CentOS-6.4-Custom
dFile=/data/files/CentOS-6.4-x86_64-CD.iso
/bin/rm -fv $dDisk/repodata/*
/bin/cp -fv $sDisk/repodata/*-c6-x86_64-comps.xml $dDisk/repodata/comps.xml
/usr/bin/createrepo -u "media://$(head -1 .discinfo)" -g $dDisk/repodata/comps.xml $dDisk/
find $dDisk -type f -name TRANS.TBL | xargs /bin/rm -fv 
cd $dDisk
mkisofs -o $dFile -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -R -J -v -input-charset=utf-8 -T $dDisk
